NAME := AWPGO
IGNORE_FAIL := || true
WAIT := sleep
ifeq ($(OS),Windows_NT)
	EXT := .exe
	IGNORE_FAIL = || (exit 0)
	#WAIT = timeout
endif

envrun:
	docker start testPostgres || docker run --name testPostgres -p 5432:5432 -e POSTGRES_PASSWORD=123 -e POSTGRES_DB=authGrpc -d postgres
	go run server/*.go
	go run client/*.go
