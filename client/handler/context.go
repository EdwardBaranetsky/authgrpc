package handler

import (
	"google.golang.org/grpc"
	"log"
	"test/authGrpc/api"
)

type Context struct {
	authClient api.AuthClient
}

func InitContext(conn *grpc.ClientConn) *Context {
	ctx := Context{
		authClient: InitClientGrpc(conn),
	}

	return &ctx
}

func InitClientGrpc(conn *grpc.ClientConn) api.AuthClient {
	conn, err := grpc.Dial("127.0.0.1:7777", grpc.WithInsecure())
	if err != nil {
		log.Fatalln("grpc dial err: ", err)
	}

	return api.NewAuthClient(conn)
}
