package handler

import (
	"context"
	"net/http"
	"test/authGrpc/api"
)

func (ctx *Context) Register(w http.ResponseWriter, r *http.Request) {

	u, err := ctx.authClient.Register(context.Background(), &api.RegisterMessage{
		Name:     r.FormValue("name"),
		Login:    r.FormValue("login"),
		Password: r.FormValue("password"),
	})
	if err != nil {
		WriteError(w, err.Error())
		return
	}

	WriteStruct(w, u)
}
