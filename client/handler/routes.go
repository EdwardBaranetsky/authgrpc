package handler

import "github.com/gorilla/mux"

func InitRoutes(ctx *Context) *mux.Router {
	router := mux.NewRouter()

	router.HandleFunc(`/register`, ctx.Register)

	return router
}
