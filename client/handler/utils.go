package handler

import (
	"encoding/json"
	"net/http"
)

func WriteError(w http.ResponseWriter, errstr string) {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.WriteHeader(http.StatusForbidden)
	json.NewEncoder(w).Encode(struct{ Error string }{errstr})
}

func WriteOK(w http.ResponseWriter) {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	json.NewEncoder(w).Encode(struct{ Status string }{"OK"})
}

func WriteStruct(w http.ResponseWriter, obj interface{}) {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	json.NewEncoder(w).Encode(obj)
}
