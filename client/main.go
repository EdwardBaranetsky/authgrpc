package main

import (
	"google.golang.org/grpc"
	"net/http"
	"test/authGrpc/client/handler"
	"time"
)

func main() {

	var conn *grpc.ClientConn

	ctx := handler.InitContext(conn)
	defer conn.Close()

	serv := &http.Server{
		Handler:      handler.InitRoutes(ctx),
		Addr:         "127.0.0.1:8888",
		ReadTimeout:  5 * time.Second,
		WriteTimeout: 30 * time.Second,
	}

	serv.ListenAndServe()
}
