package integration

import (
	"bytes"
	"mime/multipart"
	"net/http"
	"net/http/httptest"

	"github.com/gorilla/mux"
)

const (
	BaseHost = "http://127.0.0.1:8888"
)

type TestContext struct {
	Router       *mux.Router
	UserSession  string
	AdminSession string
}

func (ctx *TestContext) Clear() {
	ctx.UserSession = ""
	ctx.AdminSession = ""
}

func (ctx *TestContext) TestGET(str_url string, headers map[string]string) (*httptest.ResponseRecorder, *http.Request) {
	r := httptest.NewRequest("GET", BaseHost+str_url, nil)
	if ctx.UserSession != "" {
		r.Header.Set("Cookie", ctx.UserSession)
	}
	if ctx.AdminSession != "" {
		r.Header.Add("Cookie", ctx.AdminSession)
	}
	if headers != nil {
		for k, v := range headers {
			r.Header.Set(k, v)
		}
	}

	w := httptest.NewRecorder()
	ctx.Router.ServeHTTP(w, r)
	return w, r
}

func (ctx *TestContext) TestPOST(str_url string, headers map[string]string, params map[string]string) (*httptest.ResponseRecorder, *http.Request) {
	body := &bytes.Buffer{}
	writer := multipart.NewWriter(body)

	if params["file"] != "" {
		fileWriter, _ := writer.CreateFormFile("file", "fileName")
		fileWriter.Write([]byte(params["file"]))
		delete(params, "file")
	}

	if params != nil {
		for k, v := range params {
			writer.WriteField(k, v)
		}
	}
	writer.Close()

	r := httptest.NewRequest("POST", BaseHost+str_url, body)
	r.Header.Set("Content-Type", writer.FormDataContentType())
	if ctx.UserSession != "" {
		r.Header.Set("Cookie", ctx.UserSession)
	}
	if ctx.AdminSession != "" {
		r.Header.Add("Cookie", ctx.AdminSession)
	}

	if headers != nil {
		for k, v := range headers {
			r.Header.Set(k, v)
		}
	}

	w := httptest.NewRecorder()
	ctx.Router.ServeHTTP(w, r)
	return w, r
}
