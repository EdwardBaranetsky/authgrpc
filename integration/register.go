package integration

import (
	. "github.com/smartystreets/goconvey/convey"
	"log"
	"net/http"
)

func (ctx *TestContext) RegisterTest() {

	w, _ := ctx.TestPOST(`/register`, nil, map[string]string{
		"name":     "vasea",
		"login":    "vperson",
		"password": "123",
	})
	log.Println("response - ", w.Body.String())
	So(w.Code, ShouldEqual, http.StatusOK)

}
