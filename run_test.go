// +build integration

package main

import (
	. "github.com/smartystreets/goconvey/convey"
	"google.golang.org/grpc"
	"test/authGrpc/client/handler"
	"test/authGrpc/integration"
	"testing"
)

func TestMain(t *testing.T) {

	var conn *grpc.ClientConn

	ctx := &integration.TestContext{
		Router: handler.InitRoutes(handler.InitContext(conn)),
	}

	Convey("Test", t, func() {
		Convey("RegisterTest", func() {
			ctx.RegisterTest()
		})
	})

	if conn != nil {
		conn.Close()
	}
}
