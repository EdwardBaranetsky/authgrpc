package main

import (
	"github.com/jmoiron/sqlx"
	"github.com/pressly/goose"
	"log"
	"test/authGrpc/server/model/db"
	"test/authGrpc/server/model/user"
)

type Context struct {
	Database       *sqlx.DB
	UserController user.UserController
}

func InitContext() *Context {
	ctx := Context{}

	ctx.Database = db.InitPostgres(db.Config{
		Database: "authGrpc",
		Host:     "localhost",
		User:     "postgres",
		Password: "123",
		Port:     5432,
	})
	if ctx.Database == nil {
		log.Println("[CONTEXT_INIT] Cant connect to database!")
		return nil
	}

	err := goose.Up(ctx.Database.DB, "./server/model/db/migrations")
	if err != nil {
		log.Println("[CONTEXT_INIT] Cant run migrations (goose):", err)
		return nil
	}

	ctx.UserController = &user.UserControllerReal{
		Database: ctx.Database,
	}

	return &ctx
}
