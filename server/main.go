package main

import (
	"log"
	"net"
	"test/authGrpc/api"

	"google.golang.org/grpc"
)

func main() {
	listener, err := net.Listen("tcp", ":7777")
	if err != nil {
		log.Fatalln("cannot listen tcp, err:", err)
	}

	serverInstance := InitServer()

	grpcServer := grpc.NewServer()

	api.RegisterAuthServer(grpcServer, &serverInstance)

	if err = grpcServer.Serve(listener); err != nil {
		log.Println("grpc server err:", err)
	}
}
