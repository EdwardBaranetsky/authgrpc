package db

import (
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq" //postgres driver
	"log"
	"strconv"
)

type Config struct {
	Host     string
	Port     uint16
	User     string
	Password string
	Database string
	SSL      bool
}

func InitPostgres(cfg Config) *sqlx.DB {
	sslmode := "disable"
	if cfg.SSL {
		sslmode = "enable"
	}

	db, err := sqlx.Open("postgres",
		"user="+cfg.User+
			" password="+cfg.Password+
			" dbname="+cfg.Database+
			" sslmode="+sslmode+
			" port="+strconv.FormatUint(uint64(cfg.Port), 10)+
			" host="+cfg.Host)

	if err != nil {
		log.Println("Database Start err:", err)
		return nil
	}

	db.SetMaxOpenConns(32)
	db.SetMaxIdleConns(8)

	return db
}
