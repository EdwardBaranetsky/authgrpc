package user

import "github.com/jmoiron/sqlx"

type UserReal struct {
	id       uint64
	name     string
	login    string
	password string
}

func (u *UserReal) GetId() uint64 {
	return u.id
}

func (u *UserReal) GetName() string {
	return u.name
}

func (u *UserReal) GetLogin() string {
	return u.login
}

type UserControllerReal struct {
	Database *sqlx.DB
}

func (ctrl *UserControllerReal) Create(data CreateData) (User, error) {

	u := UserReal{
		name:     data.Name,
		login:    data.Login,
		password: data.Password,
	}

	err := ctrl.Database.Get(&u.id,
		`INSERT INTO users(name, login, password) VALUES($1,$2,$3) RETURNING id`,
		data.Name, data.Login, data.Password)

	return &u, err
}
