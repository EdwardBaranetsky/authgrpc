package user

type User interface {
	GetId() uint64
	GetName() string
	GetLogin() string
}

type UserController interface {
	Create(data CreateData) (User, error)
}

type CreateData struct {
	Login    string
	Password string
	Name     string
}
