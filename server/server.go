package main

import (
	"context"
	"test/authGrpc/api"
	"test/authGrpc/server/model/user"
)

type Server struct {
	*Context
}

func InitServer() Server {
	srv := Server{}
	srv.Context = InitContext()
	return srv
}

func (srv *Server) Register(ctx context.Context, message *api.RegisterMessage) (*api.User, error) {

	u, err := srv.UserController.Create(user.CreateData{
		Password: message.Password,
		Login:    message.Login,
		Name:     message.Name,
	})
	if err != nil {
		return nil, err
	}

	return &api.User{Id: u.GetId(), Name: u.GetName()}, nil
}
